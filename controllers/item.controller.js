const mongoose = require('mongoose');
const Item = mongoose.model('Item');
const User = mongoose.model('User');


module.exports.postItem = function (req, res) {
    //Build the item
    const item = new Item(req.body);
    item.setIdHash(req.body.itemId);
    console.log(item);

    //Retrieve the user associated through his token
    const ownerId = req.user._id;

    User.findOne(
        {_id: ownerId},
        function (err, user) {
            if (user) {
                user.items.push(item);
                user.save()
                    .then(item => {
                        res.status(200);
                        res.json({item: item})
                    })
                    .catch(error => {
                        res.status(400);
                        res.send("Can't store item on the database: " + error
                        );
                    })
            }
        }
    );


};


module.exports.deleteItem = function (req, res) {
    const itemId = req.params.item_id;
    User.findOneAndUpdate(
        {'items.itemId': itemId},
        {$pull: {items: {itemId: itemId}}},
        {safe: true, upsert: true, new:true}
    )
        .exec(function(err, data){
            if(err){
                console.log(err)
            }
            res.status(200).json(data.items)
        })
};


module.exports.getItemOwner = function (req, res) {
    const itemIdHash = req.params.item_id_hash;
    User.findOne({'items.idHash': itemIdHash})
        .exec( function(err, user){
            if(err){
                console.log(err)
            }
            console.log(user);
            res.status(200).json(user)
        })
};
