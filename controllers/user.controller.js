const mongoose = require('mongoose');
const User = mongoose.model('User');

/**
 * Function in order to collect the items of a given user
 * @endpoint   :/users/user_id/items
 * @param req  :The request object
 * @param res  :The result object
 */
module.exports.getUserItems = function (req, res) {
    const userId = req.params.user_id;
    User.findOne({_id: userId})
        .exec(function (err, user) {
            if(err){
                console.log(err);
            }
            res.status(200).json(user.items)
        })

};

/**
 * Get all the user
 * @param req: The request
 * @param res: The result
 * @endpoint : /users
 */
module.exports.getUsers = function (req, res) {
    User
        .find()
        .exec(function (err, users) {
            if(err){
                console.log(err);
            }
            res.status(200).json(users)
        })
};

/**
 * Get a user according to the given user_id
 * @param req :The request object
 * @param res :The result object
 * @endpoint : /users/user_id
 */
module.exports.getUser = function (req, res) {
    const user_id = req.params.user_id;
    User
        .findOne({_id: user_id})
        .exec(function (err, user) {
            if(err){
                console.log(err);
            }
            res.status(200).json(user)
        });
};
