const passport = require('passport');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const ContactDetails = mongoose.model('ContactDetails');
const BankingDetails = mongoose.model('BankingDetails');

let sendJSONresponse = function (res, status, content) {
  res.status(status);
  res.json(content);
};

/**
 * Function in order to register a new user
 * @param req
 * @param res
 */
module.exports.register = function (req, res) {
  let user = new User();
  user.contactDetails = new ContactDetails({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    phoneNumber: req.body.phoneNumber
  });
  user.role = 'USER';
  user.setPassword(req.body.password);
  user.save()
      .then(user => {
        const token = user.generateJwt();
        res.status(200);
        res.json({
          "token": token
        })
      })
      .catch(err => {
        if (err.code === 11000) {
          res.status(400);
          res.send(
              "Cette adresse mail est déjà utilisée"
          );
        } else {
          res.status(400);
          res.send("Impossible de stocker dans la base de données: " + err);
        }
      });
};

/**
 * Function in order to logged in a user
 * @param req
 * @param res
 */
module.exports.login = function (req, res) {
  passport.authenticate('local', function (err, user, info) {
    // console.log(user);
    let token;

    //If encountered an error:
    if (err) {
      res.status(404).json(err);
    }

    // If authentication succeed:
    else if (user) {
      token = user.generateJwt();
      res.status(200);
      res.json({
        "token": token
      });
    }

    // If user is not found
    else {
      res.status(401).json(info);
    }
  })(req, res);
};


