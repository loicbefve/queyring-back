const mongoose = require('mongoose');
const Transaction = mongoose.model('Transaction');
const ContactDetails = mongoose.model('ContactDetails');
const BankingDetails = mongoose.model('BankingDetails');
const Address = mongoose.model('Address');
const User = mongoose.model('User');
let emailCtl = require('../controllers/email.controller');


module.exports.postTransaction = function (req, res) {
    // console.log(req);
    let transaction = new Transaction(
        {
            finder: new ContactDetails(
                {
                    firstName: req.body.finder.firstName,
                    lastName: req.body.finder.lastName,
                    email: req.body.finder.email,
                    phoneNumber: req.body.finder.phoneNumber
                }
            ),
            itemDescription: req.body.itemDescription,
            meetInformation: req.body.meetInformation,
            bankingDetails: new BankingDetails({
                iban: req.body.bankingDetails.iban
            }),
            ownerId: req.body.ownerId,
            creationDate: new Date(),
            updateDate: new Date(),
            state: req.body.state

        });
    transaction.save()
        .then(transaction => {

            User
                .findOne({_id: req.body.ownerId})
                .exec(function (err, user) {

                    //In case of error
                    if (err) return console.log(err);

                    console.log(`Found email is: ${user.contactDetails.email}`);

                    //Send mail
                    emailCtl
                        .sendFinderEmail(
                            [user.contactDetails.email],
                            user.contactDetails.firstName,
                            req.body.itemDescription
                        )
                        .then((result)=> console.log(result))
                        .catch((err) => {
                            console.error('Something went wrong with Finder mail sending:');
                            console.error(err)
                        });

                });

            res.status(201).json(transaction);
        })
        .catch(err => {
            res.status(500);
            res.send("Can't save transaction on database: " + err);
        });

};

module.exports.getUserTransactions = function (req, res) {
    let user_id = req.params.user_id;
    Transaction
        .find({'ownerId': user_id})
        .exec(function (err, transactions) {
            if (err) {
                console.log(err)
            }
            // console.log(transactions);
            res.status(200).json(transactions)
        })
};

module.exports.patchTransactionState = function (req, res) {

    const transaction_id = req.params.transaction_id;

    const new_state = req.body.state;

    Transaction
        .findOneAndUpdate(
            {'_id': transaction_id},
            {
                state: req.body.state,
                updateDate: Date.now()
            },
            {new: true}
        )
        .exec(function (err, transaction) {
            if (err) {
                console.log(err)
            }
            if (new_state === 'PAID') {
                emailCtl.sendFinderEmail()
                    .catch((err) => {
                        console.error('Something went wrong with Finder mail sending:');
                        console.error(err)
                    });
            }
            res.status(200).json(transaction)
        })
};
