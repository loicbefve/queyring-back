"use strict";
const nodemailer = require("nodemailer");
const path = require('path');
const hbs = require('nodemailer-express-handlebars');


// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    secure: process.env.EMAIL_SECURE === 'true', // true for 465, false for other ports
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PWD
    },
    logger: true
});

transporter.use('compile', hbs(
    {
        viewEngine: {
            extname: '.hbs',
            partialsDir: path.resolve(__dirname, '../views/email'),
            layoutsDir: path.resolve(__dirname, '../views/email'),
            defaultLayout: 'main.handlebars'
        },
        extname: '.hbs',
        viewPath: path.resolve(__dirname, '../views/email')

    }
));


module.exports.sendFinderEmail = async function (dests, name, description) {

    // send mail with defined transport object
    const found_mail = {
        from: '"Loïc de QueyRing" <no-reply@queyring.com>', // sender address
        to: dests.join(", "), // list of receivers
        subject: "Votre objet a été retrouvé!", // Subject line
        template: 'item_found_mail',
        context: {
            name: name,
            description: description
        }
    };

    transporter.sendMail(found_mail, (err, info, response) => {
        if( err ){
            console.error(err)
        } else if( info ) {
            console.info(info)
        }
    })

};
