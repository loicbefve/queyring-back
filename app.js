/**
 *Imports
 */
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
//Handle cross sites requests
const cors = require('cors');
//Handle authentication
const passport = require('passport');
//Security module
const helmet = require('helmet');


/**
 * Config files
 */
//Load environment variables
require('dotenv').config();
//Load the database configuration
require('./config/DB');
//Load the passport configuration
require('./config/passport');


/**
 * Routers import
 */
const routesUser = require('./routes/user.route');
const routesItem = require('./routes/item.route');
const routesAuth = require('./routes/auth.route');
const routesTransaction = require('./routes/transaction.route');


/**
 * JWT import
 */
const jwt = require('express-jwt');

/**
 * App import
 */
const app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(helmet());

app.use(logger('dev'));

app.use(express.json());

app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());



// User has to be connected for all endpoints
// except for register and login ones
app.use(jwt(
    {
        secret: process.env.JWT_SECRET
    })
    .unless(
        {
            path: [
                '/auth/login',
                '/auth/register',
                /^\/items\/[0-9a-z]+\/owner/,
                /^\/transactions.*/
            ]
        })
);// TODO: Secret provider to implement


app.use(passport.initialize({}));


/**
 * Routes use
 */
app.use('/auth', routesAuth);
app.use('/users', routesUser);
app.use('/items', routesItem);
app.use('/transactions', routesTransaction);

app.use((req, res, next)=> {
    // console.log('yes');
    next()
});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

//invalid jwt handler
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401).send('invalid token...');
    } else {
        res.status(err.status || 500).send(`${err.message}`);
    }
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500).send(`${err.message}`);
    // res.render('error');
});

module.exports = app;
