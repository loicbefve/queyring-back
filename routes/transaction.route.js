const express = require('express');
const router = express.Router();
//var ctrlProfile = require('../controllers/profile');
let ctrlTrans = require('../controllers/transaction.controller');


/** Post routes */

/**
 * Route to post a new transaction
 * @endpoint : /transactions
 */
router.post('', ctrlTrans.postTransaction );


router.get('/:user_id', ctrlTrans.getUserTransactions);

router.patch('/state/:transaction_id', ctrlTrans.patchTransactionState);

module.exports = router;
