const express = require('express');
const router = express.Router();
//var ctrlProfile = require('../controllers/profile');
let ctrlItem = require('../controllers/item.controller');


/**Post routes ***************************/

/**
 *  The route to add a new item: /items
 *  @endpoint: /items
 */
router.post('/', ctrlItem.postItem);


/** Delete routes **************************************/


/**
 *  The route to delete a given item (oid), owned by uid
 *  @endpoint: items/:item_id
 */
router.delete('/:item_id', ctrlItem.deleteItem);



/** Get routes ***************************************/


/**
 *  Get owner of an item
 *  @endpoint: items/:item_id_hash/owner
 */
router.get("/:item_id_hash/owner", ctrlItem.getItemOwner);



module.exports = router;
