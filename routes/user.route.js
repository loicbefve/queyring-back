const express = require('express');
const router = express.Router();
const guard = require('express-jwt-permissions')();
const jwt = require('express-jwt');


//var ctrlProfile = require('../controllers/profile');

let ctrlUser = require('../controllers/user.controller' );



/** Get routes *****************************************************/

/**
 * Get all the users on the database
 * only accessible for an ADMIN
 * @endpoint: /users
 */
router.get('/', guard.check('ADMIN'), ctrlUser.getUsers);

/**
 * Get a specific user according to his id
 * @endpoint: /users/:user_id
 */
router.get('/:user_id', ctrlUser.getUser);

/**
 *  Get all the items owned by a user
 *  identified by his user id
 *  @endpoint: /users/:user_id/items
 */
router.get('/:user_id/items', ctrlUser.getUserItems);

module.exports = router;
