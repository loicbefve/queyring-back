const express = require('express');
const router = express.Router();

let ctrlAuth = require('../controllers/authentication.controller');

/** Post routes */
router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);

module.exports = router;
