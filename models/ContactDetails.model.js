const mongoose = require('mongoose');
const AddressModel = require('./Address.model');


const contactSchema = new mongoose.Schema({
        firstName: {
            type: String,
            required: [true, 'First name is required!'],
            max: [128, 'The maximum length of this field is 128'],
            validate: {
                validator: function (v) {
                    return /^[\D]+$/.test(v);
                },
                message: props => `${props.value} is not a valid first name!`
            }
        },
        lastName: {
            type: String,
            required: [true, 'First name is required!'],
            max: [128, 'The maximum length of this field is 128'],
            validate: {
                validator: function (v) {
                    return /^[\D]+$/.test(v);
                },
                message: props => `${props.value} is not a valid last name!`
            }
        },
        address: {
            type: AddressModel.schema,
            required: [false]
        },
        email: {
            type: String,
            required: [true, 'valid email is required!'],
            validate: {
                validator: function (v) {
                    // TODO: Attention ce validator est différent de celui présent en front
                    return /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/.test(v);
                },
                message: props => `${props.value} not a valid email!`
            }
        },
        phoneNumber: {
            type: String,
            min: [10, 'Phone number must have 10 digits'],
            max: [10, 'Phone number must have 10 digits'],
            validate: {
                validator: function (v) {
                    return /[0-9]{10}/.test(v);
                },
                message: props => `${props.value} is not a valid phone number!`
            }
        }
    }, {autoCreate: false}
);

module.exports = mongoose.model('ContactDetails', contactSchema);
