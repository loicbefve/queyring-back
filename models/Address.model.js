const mongoose = require('mongoose');

const addressSchema = new mongoose.Schema(
    {
        streetNumber: {
            type: String,
            required: [true, 'Street number is required'],
            max: [10, 'The maximum length of this field is 10'],
        },
        street: {
            type: String,
            required: [true, 'Street is required'],
            max: [40, 'The maximum length of this field is 40'],
        },
        streetComplement: {
            type: String,
            max: [50, 'The maximum length of this field is 50'],
        },
        zipCode: {
            type: String,
            required: [true, 'ZipCode is required'],
            max: [10, 'The maximum length of this field is 10'],
        },
        city: {
            type: String,
            required: [true, 'City is required'],
            max: [50, 'The maximum length of this field is 50'],
        },
        country: {
            type: String,
            required: [true, 'Country is required'],
            max: [80, 'The maximum length of this field is 80'],
        }
    },
    {
        autoCreate:false
    }
);

module.exports = mongoose.model('Address', addressSchema);

