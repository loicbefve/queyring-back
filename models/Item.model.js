//User.model.js
const crypto = require('crypto');
const mongoose = require('mongoose');

//Define collection and schema for Item
let itemSchema = new mongoose.Schema({
    itemId: {
        type: String,
        unique: true,
        sparse: true,
        required: [true, 'itemId is required'],
        validate: {
            validator: function (v) {
                return /[0-9a-z]{15}/.test(v);
            },
            message: props => `${props.value} is not a valid itemId!`
        }
    },
    idHash: {
      type: String,
      required: [true, 'idHAsh is required']
    },
    itemPass: {
        type: String,
        required: [true, 'itemPass is required'],
        validate: {
            validator: function (v) {
                return /[0-9]{2}/.test(v);
            },
            message: props => `${props.value} is not a valid itemPass!`
        }
    },
    itemDescription: {
        type: String,
        required: [true, 'itemDescription is required!'],
        max: [128, 'itemDescription 128 characters max']
    }
}, {autoCreate: false});

itemSchema.methods.setIdHash = function( itemId ) {

    this.idHash = crypto
        .pbkdf2Sync(
            itemId,
            process.env.LINK_SALT,
            1000,
            32,
            'sha512'
        )
        .toString('hex');
};

module.exports = mongoose.model('Item', itemSchema);
