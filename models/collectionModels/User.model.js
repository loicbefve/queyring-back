//User.model.js

const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const ItemModel = require('../Item.model');
const ContactDetailsModel = require('../ContactDetails.model');
const BankingDetailsModel = require('../BankingDetails.model');


//Define collection and schema for User
let userSchema = new mongoose.Schema({
    contactDetails: ContactDetailsModel.schema,
    role: {
        type: String,
        required: [true, 'A role should be defined'],
        validate: {
            validator: function (v) {
                return /^(ADMIN|USER)$/.test(v)
            },
            message: props => `${props.value} is not a valid role!`
        }
    },
    items: [ItemModel.schema],
    bankingDetails: BankingDetailsModel.schema,
    hash: String,
    salt: String
}, {
    collection: 'user'
});

userSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto
        .pbkdf2Sync(
            password,
            this.salt,
            1000,
            64,
            'sha512')
        .toString('hex');
};

userSchema.methods.validPassword = function (password) {
    const hash = crypto.pbkdf2Sync(
        password,
        this.salt,
        1000,
        64,
        'sha512'
    ).toString('hex');

    return this.hash === hash
};

userSchema.methods.generateJwt = function () {
    let expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign(
        {
            _id: this._id,
            email: this.email,
            firstName: this.firstName,
            permissions: [
                this.role
            ],
            exp: parseInt(expiry.getTime() / 1000),
        },
        process.env.JWT_SECRET
    );
};


module.exports = mongoose.model('User', userSchema);
