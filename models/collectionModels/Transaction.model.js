const mongoose = require('mongoose');
const ContactDetailsModel = require('../ContactDetails.model');
const BankingDetailsModel = require('../BankingDetails.model');


const transactionSchema = new mongoose.Schema({
        finder: ContactDetailsModel.schema,
        itemDescription: {
            type: String,
            required: [true, 'Item description is required!'],
            max: [128, 'The maximum length of this field is 128'],
        },
        meetInformation: {
            type: String,
            required: [true, 'Meet information is required!'],
            max: [200, 'The maximum length of this field is 200'],
        },
        bankingDetails: BankingDetailsModel.schema,
        ownerId: {
            type: String,
            required: [true, 'ownerId is required']
        },
        creationDate: {
            type: Date,
            required: [true, 'creationDate is required']
        },
        updateDate: {
            type: Date,
            required: [true, 'updateDate is required']
        },
        state: {
            type: String,
            required: [true, 'state is required']
        }
    },{collection: 'transactions'}
);

module.exports = mongoose.model('Transaction', transactionSchema);
