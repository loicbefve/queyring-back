const mongoose = require('mongoose');
const crypto = require('crypto');

const bankingDetailsSchema = new mongoose.Schema(
    {
        iban: String,
    },
    {
        autoCreate: false
    }
);

module.exports = mongoose.model('BankingDetails', bankingDetailsSchema);
